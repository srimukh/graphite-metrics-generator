package com.miracle.metricsgenerator;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import org.springframework.boot.actuate.metrics.Metric;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Meter;
import com.codahale.metrics.Metered;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Snapshot;
import com.codahale.metrics.Timer;

/**
 * <h1>MetricCollector</h1> 
 * 
 * <p>
 * This class is used to collect all the metrics
 * </p>
 * 
 * @author GBOS Integration Technologies
 * @version 0.1
 * @since 20 June,2016
 */



public class MetricCollector {
	
	private MetricNamer	    namer;
	private MetricRegistry	registry;
	private List<Metric<?>>	metrics	= new ArrayList<>();
	
	/**
	 * Parameterized Constructor to set the values for MetricCollector
	 * 
	 * @param metricNamer
	 * 		- MetricNamer which holds the metricNamer
	 * @param metricRegistry
	 * 		- MetricRegistry which holds the metricRegistry
	 *
	 **/
	public MetricCollector(MetricNamer namer, MetricRegistry registry) {
		this.namer = namer;
		this.registry = registry;
	}
	
	/**
	 * This method is used to obtain the metrics
	 * 
	 * @return the ArrayList<Metric<?>>
	 *
	 **/
	public List<Metric<?>> getMetrics() {
		addTimers();
		addMeters();
		addGauges();
		addCounters();
		return metrics;
	}
	
	/**
	 * This method is used to add timers
	 * 	
	 *
	 **/
	private void addTimers() {
		SortedMap<String, Timer> timers = registry.getTimers();	
		for (Map.Entry<String, Timer> entry : timers.entrySet())
		{
			Timer timer = entry.getValue();					
			if ((timer != null) && (timer.getCount() > 0))
			{
				addTimerMetrics(entry.getKey(), timer);
			}
		}
	}
	
	/**
	 * This method is used to add timer to metrics
	 * 
	 * @param key
	 * 		- String which holds the key
	 * @param timer
	 * 		- Timer which holds the timer
	 *
	 **/
	private void addTimerMetrics(String key, Timer timer) {
		addMeterMetrics(key, timer);
		
		Snapshot snapshot = timer.getSnapshot();
		addMetric("timer.min", key, snapshot.getMin());
		addMetric("timer.max", key, snapshot.getMax());
		addMetric("timer.median", key, snapshot.getMedian());
		addMetric("timer.mean", key, snapshot.getMean());
		addMetric("timer.standard-deviation", key, snapshot.getStdDev());
	}
	
	/**
	 * This method is used to add meters	
	 *
	 **/
	private void addMeters() {
		SortedMap<String, Meter> meters = registry.getMeters();
		for (Map.Entry<String, Meter> entry : meters.entrySet())
		{
			Meter meter = entry.getValue();
			
			if ((meter != null) && (meter.getCount() > 0))
			{
				addMeterMetrics(entry.getKey(), meter);
			}
		}
	}
	
	/**
	 * This method is used to add meter to metrics
	 * 
	 * @param key
	 * 		- String which holds the key
	 * @param meter
	 * 		- Metered which holds the meter
	 *
	 **/
	private void addMeterMetrics(String key, Metered meter) {
		addMetric("meter.mean", key, meter.getMeanRate());
		addMetric("meter.one-minute", key, meter.getOneMinuteRate());
		addMetric("meter.five-minute", key, meter.getFiveMinuteRate());
		addMetric("meter.fifteen-minute", key, meter.getFifteenMinuteRate());
	}
	
	/**
	 * This method is used to add guages	
	 *
	 **/
	@SuppressWarnings("rawtypes")
	private void addGauges() {
		
		SortedMap<String, Gauge> gauges = registry.getGauges();
		for (Map.Entry<String, Gauge> entry : gauges.entrySet())
		{
			Gauge gauge = entry.getValue();
			
			if ((gauge != null) && (gauge.getValue() instanceof Number))
			{
				addMetric("gauge", entry.getKey(), (Number) gauge.getValue());
			}
		}
	}
	
	/**
	 * This method is used to add counters	
	 *
	 **/
	private void addCounters() {
		SortedMap<String, Counter> counters = registry.getCounters();
		for (Map.Entry<String, Counter> entry : counters.entrySet())
		{
			Counter counter = entry.getValue();
			
			if (counter != null)
			{
				addMetric("counter", entry.getKey(), counter.getCount());
			}
		}
	}
	
	/**
	 * This method is used to add metric	
	 * @param metricType
	 * 		- String which holds the metricType
	 * @param key
	 * 		- String which holds the key
	 * @param metric
	 * 		- T which holds the metric
	 * 	 
	 **/
	private <T extends Number> void addMetric(String metricType, String key, T metric) {
		String name = namer.getMetricName(metricType, key);
		metrics.add(new Metric<>(name, metric));
	}


}
