package com.miracle.metricsgenerator;
/**
 * <h1>MetricCollector</h1> 
 * 
 * <p>
 * This class is used to name the metric
 * </p>
 * 
 * @author GBOS Integration Technologies
 * @version 0.1
 * @since 20 June,2016
 */



public class MetricNamer {


	private String serviceName;
	private String trimString;
	/**
	 * Default Constructor
	 **/
	public MetricNamer() {
		serviceName = "unknown";
		trimString = "unknown";
	}
	/**
	 * Parameterized Constructor to set the values for MetricNamer
	 * 
	 * @param serviceName
	 * 		- String which holds the serviceName
	 * @param trimString
	 * 		- String which holds the trimString
	 *
	 **/
	public MetricNamer(String serviceName, String trimString) {
		this.serviceName = serviceName;
		this.trimString = trimString;
	}
	/**
	 * This method is used to get the metric name
	 * 
	 * @param metricType
	 * 		- String which holds the metricType
	 * @param metricKey
	 * 		- String which holds the metricKey
	 * @return metricName
	 **/
	public String getMetricName(String metricType, String metricKey) {
		String shortKeyName = convertKeyToShortName(metricKey);
		return serviceName + "." + metricType + "." + shortKeyName;
	}
	/**
	 * This method is used to convert key to shortKeyName
	 * 
	 * @param key
	 * 		- String which holds the key
	 *
	 * @return shortKeyName
	 **/
	private String convertKeyToShortName(String key) {
		String shortKeyName = key;

		if (key.startsWith(trimString)) {
			String[] segments = key.split("\\.");
			if (segments.length > 1) {
				shortKeyName = segments[segments.length - 2] + "." + segments[segments.length - 1];
			}
		}
		return shortKeyName;
	}

	
}
