package com.miracle.metricsgenerator;

import java.util.Collection;

import org.springframework.boot.actuate.endpoint.PublicMetrics;
import org.springframework.boot.actuate.metrics.Metric;

import com.codahale.metrics.MetricRegistry;
/**
 * <h1>CodahaleMetricsAdapter</h1> 
 * 
 * <p>
 * This class acts as an adapter to implement the metrics
 * </p>
 * 
 * @author GBOS Integration Technologies
 * @version 0.1
 * @since 20 June,2016
 */


	public class CodahaleMetricsAdapter implements PublicMetrics {

		private MetricNamer metricNamer;
		private MetricRegistry metricRegistry;
		/**
		 * Parameterized Constructor to set the values for CodahaleMetricsAdapter
		 * 
		 * @param metricNamer
		 * 		- MetricNamer which holds the metricNamer
		 * @param metricRegistry
		 * 		- MetricRegistry which holds the metricRegistry
		 *
		 **/
		public CodahaleMetricsAdapter(MetricNamer metricNamer, MetricRegistry metricRegistry) {
			this.metricNamer = metricNamer;
			this.metricRegistry = metricRegistry;
		}
		/**
		 * This method is used to obtain the metrics
		 * 
		 * @return the collection of metrics
		 *
		 **/
		@Override
		public Collection<Metric<?>> metrics() {
			MetricCollector metrics = new MetricCollector(metricNamer, metricRegistry);
			return metrics.getMetrics();
		}
	}

	
	
	
	

