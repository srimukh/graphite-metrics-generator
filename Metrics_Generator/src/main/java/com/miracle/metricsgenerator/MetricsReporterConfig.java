package com.miracle.metricsgenerator;


import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Configuration;
import org.springframework.util.FileSystemUtils;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.CsvReporter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.MetricSet;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.codahale.metrics.jvm.FileDescriptorRatioGauge;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
import com.ryantenney.metrics.spring.config.annotation.EnableMetrics;
import com.ryantenney.metrics.spring.config.annotation.MetricsConfigurerAdapter;


/**
 * <h1>MetricsReporterConfig</h1>
 * 
 * <p>
 * This class is used to report the metric configuration
 * </p>
 * 
 * @author GBOS Integration Technologies
 * @version 0.1
 * @since 20 June,2016
 */
@EnableMetrics
@Configuration


public class MetricsReporterConfig extends MetricsConfigurerAdapter {

	// Default reporter
	private static Reporter	      reporter	                = Reporter.GRAPHITE;
	public static final String	  ip	                    = "13.84.52.95";
	final Graphite	              graphite	                = new Graphite(new InetSocketAddress(ip, 8080));
	private static MetricRegistry	metricRegistry1	        = new MetricRegistry();
	public static final int	      CONFIGURATION_REPORT_TIME	= 20;
	
	/**
	 * This method is used to configure the reporters
	 * 
	 * @param metricRegistry
	 *            - MetricRegistry which holds the metricRegistry
	 * 
	 **/
	@Override
	public void configureReporters(MetricRegistry metricRegistry) {
		metricRegistry1 = metricRegistry;
		registerJVMMetrics(metricRegistry1);
				registerReporter(
				        GraphiteReporter
				                .forRegistry(metricRegistry1)
				                .prefixedWith(
				                        MetricsReporterConfig.getHostName()
				                                + "activemqtest")
				                .convertRatesTo(TimeUnit.SECONDS)
				                .convertDurationsTo(TimeUnit.MILLISECONDS)
				                .filter(MetricFilter.ALL).build(graphite)).start(
				        CONFIGURATION_REPORT_TIME, TimeUnit.SECONDS);		
		
	}
	
	/**
	 * This method is used to register the metrics for JVM
	 * 
	 * @param metricRegistry
	 *            - MetricRegistry which holds the metricRegistry
	 * 
	 **/
	private void registerJVMMetrics(MetricRegistry metricRegistry) {
		MetricSet jvmMetrics = new MetricSet() {
			
			@Override
			public Map<String, com.codahale.metrics.Metric> getMetrics() {
				
				Map<String, com.codahale.metrics.Metric> metrics = new HashMap<String, com.codahale.metrics.Metric>();
				metrics.put("jvm.gc", new GarbageCollectorMetricSet());
				metrics.put("jvm.file-descriptors",
				        new FileDescriptorRatioGauge());
				metrics.put("jvm.memory-usage", new MemoryUsageGaugeSet());
				metrics.put("jvm.threads", new ThreadStatesGaugeSet());
				return metrics;
			}
		};
		metricRegistry.registerAll(jvmMetrics);
	}
	
	private static final String	HOSTNAME_DEFAULT	= "unknown-host";
	
	/**
	 * This method is used to obtain host name
	 * 
	 * @return hostName
	 * 
	 **/
	public static String getHostName() {
		String hostname = HOSTNAME_DEFAULT;
		
		if (System.getProperty("os.name").startsWith("Windows"))
		{
			// Windows will always set the 'COMPUTERNAME' variable
			hostname = System.getenv("COMPUTERNAME");
		}
		else
		{
			hostname = getHostnameOfNonWindowsSystem();
		}
		
		return getHostnameWithoutDomainName(hostname);
	}
	
	/**
	 * This method is used to obtain host name for windows system
	 * 
	 * @return hostName
	 * 
	 **/
	private static String getHostnameOfNonWindowsSystem() {
		
		String hostname = System.getenv("HOSTNAME");
		if (hostname != null)
		{
			return hostname;
		}
		else
		{
			return getLocalHostname();
		}
	}
	
	/**
	 * This method is used to return the host name
	 * 
	 * @return hostName
	 * 
	 **/
	private static String getLocalHostname() {
		try
		{
			return InetAddress.getLocalHost().getHostName();
		}
		catch (Exception exception)
		{
			return HOSTNAME_DEFAULT;
		}
	}
	
	/**
	 * This method is used to obtain host name without domain name
	 * 
	 * @return hostName
	 * 
	 **/
	private static String getHostnameWithoutDomainName(String hostname) {
		return hostname.replaceAll("[.].*", "");
	}
	
	/**
	 * @return the graphite
	 */
	public Graphite getGraphite() {
		return graphite;
	}
	
	/**
	 * @return the reporter
	 */
	public static Reporter getReporter() {
		return reporter;
	}
	
	/**
	 * @param reporter
	 *            the reporter to set
	 */
	public static void setReporter(Reporter reporter) {
		MetricsReporterConfig.reporter = reporter;
	}
	
	/**
	 * @return the metricRegistry
	 */
	public static MetricRegistry metricRegistry() {
		return metricRegistry1;
	}

	
}
